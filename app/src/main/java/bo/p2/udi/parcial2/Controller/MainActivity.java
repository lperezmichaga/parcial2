package bo.p2.udi.parcial2.Controller;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;

import java.util.ArrayList;
import java.util.List;

import bo.p2.udi.parcial2.Controller.Adapter.AdapterCliente;
import bo.p2.udi.parcial2.Controller.FIREBASE.FirebaseController;
import bo.p2.udi.parcial2.Controller.Interface.RecyclerView_ClickListener;
import bo.p2.udi.parcial2.Controller.SERVICE.ClienteService;
import bo.p2.udi.parcial2.Model.ClsCliente;
import bo.p2.udi.parcial2.Model.ListaCliente;
import bo.p2.udi.parcial2.R;

public class MainActivity extends AppCompatActivity implements RecyclerView_ClickListener {
    private RecyclerView DetalleAgendaRV;
    private AdapterCliente adapterCliente;
    private ListaCliente listaAgenda;

    private ClienteService clienteService;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goCreateTarea();
            }
        });

        clienteService = new ClienteService(this);
        listaAgenda =  new ListaCliente();
        this.LoadRecyclerView();
        this.CreateDataAgenda();//FIREBASE
    }


    public void goCreateTarea(){
        startActivity(new Intent(this,CreateClienteActivity.class));
    }
    private void LoadRecyclerView(){
        DetalleAgendaRV = (RecyclerView)findViewById(R.id.rvDetalleCliente);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this ,LinearLayoutManager.VERTICAL, false);
        DetalleAgendaRV.setLayoutManager(layoutManager);
        adapterCliente =  new AdapterCliente(this, listaAgenda);
        adapterCliente.setClickListener(this);
        DetalleAgendaRV.setAdapter(adapterCliente);
    }

    private void CreateDataAgenda(){
           clienteService.GetListClienteListener( new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                ClsCliente cliente = dataSnapshot.getValue(ClsCliente.class);
                listaAgenda.getClientList().add(cliente);
                adapterCliente.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                ClsCliente cliente = dataSnapshot.getValue(ClsCliente.class);
                listaAgenda.ActualizarItem(cliente);
                adapterCliente.notifyDataSetChanged();

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                ClsCliente cliente = dataSnapshot.getValue(ClsCliente.class);
                listaAgenda.RemoveItem(cliente);
                adapterCliente.notifyDataSetChanged();

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onClick(View view, int position) {
        ClsCliente client = listaAgenda.getClientList().get(position);
        Intent intent = new Intent(this,DetalleClienteActivity.class);
        intent.putExtra("IdCliente",client.getId());
        intent.putExtra("Direccion",client.getDireccion());
        intent.putExtra("Ci",client.getCi());
        intent.putExtra("Nit",client.getNit());
        intent.putExtra("Nombre",client.getNombre());
        intent.putExtra("Telefono",client.getTelefono());
        startActivity(intent);
    }
}
