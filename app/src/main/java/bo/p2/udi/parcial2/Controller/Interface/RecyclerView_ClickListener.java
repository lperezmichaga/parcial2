package bo.p2.udi.parcial2.Controller.Interface;

import android.view.View;

public interface RecyclerView_ClickListener {

    void onClick(View view, int position);

}