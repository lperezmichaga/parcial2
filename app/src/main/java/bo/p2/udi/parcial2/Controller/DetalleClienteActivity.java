package bo.p2.udi.parcial2.Controller;

import android.app.Dialog;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import bo.p2.udi.parcial2.Controller.FIREBASE.FirebaseController;
import bo.p2.udi.parcial2.Controller.SERVICE.ClienteService;
import bo.p2.udi.parcial2.Model.ClsCliente;
import bo.p2.udi.parcial2.R;

public class DetalleClienteActivity extends AppCompatActivity {
    private ClienteService clienteService;
    private String Id, Nombre, Direccion, Ci, Nit, Telefono;
    private EditText txtNombre, txtTelefono, txtCi, txtNit, txtDireccion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detallecliente);
        clienteService = new ClienteService(this);
        InitComponent();
        LoadData();
    }

    private void InitComponent() {

        Id = (getIntent() != null) ? getIntent().getExtras().getString("IdCliente") : "";
        Direccion = (getIntent() != null) ? getIntent().getExtras().getString("Direccion") : "";
        Ci = (getIntent() != null) ? getIntent().getExtras().getString("Ci") : "";
        Telefono = (getIntent() != null) ? getIntent().getExtras().getString("Telefono") : "";
        Nit = (getIntent() != null) ? getIntent().getExtras().getString("Nit") : "";
        Nombre = (getIntent() != null) ? getIntent().getExtras().getString("Nombre") : "";

        txtNombre = (EditText) findViewById(R.id.txtEdNombre);
        txtCi = (EditText) findViewById(R.id.txtEdCi);
        txtTelefono = (EditText) findViewById(R.id.txtEdTelefono);
        txtNit = (EditText) findViewById(R.id.txtEdNit);
        txtDireccion = (EditText) findViewById(R.id.txtEdDireccion);
    }

    private void LoadData() {
        txtNombre.setText(Nombre);
        txtCi.setText(Ci);
        txtDireccion.setText(Direccion);
        txtNit.setText(Nit);
        txtTelefono.setText(Telefono);
    }

    public void BtnModificatCliente(View view) {
        ClsCliente cliente = new ClsCliente();
        cliente.setId(Id);
        cliente.setNombre(txtNombre.getText().toString());
        cliente.setCi(txtCi.getText().toString());
        cliente.setDireccion(txtDireccion.getText().toString());
        cliente.setNit(txtNit.getText().toString());
        cliente.setTelefono(txtTelefono.getText().toString());
        if(ValidarCampos()) {
            clienteService.ActualizarClient(cliente, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                    if (databaseError != null) {
                        ShowAlertDialogOK("Error", databaseError.getMessage(), false);
                    } else {
                        ShowAlertDialogOK("Actulizacion Correcta", "Los Datos de los clientes han sido correctamente actualizados", true);
                    }

                }
            });
        }
    }
    public boolean ValidarCampos(){


        if(txtNombre.getText().toString().isEmpty()){
            ShowAlertDialogOK("Alerta","El Campo Nombre No puede quedar vacio",false);
            return  false;
        }
        else if(txtCi.getText().toString().isEmpty()){
            ShowAlertDialogOK("Alerta","El Campo Ci No puede quedar vacio",false);
            return  false;
        }
        else if(txtTelefono.getText().toString().isEmpty()){
            ShowAlertDialogOK("Alerta","El Campo Telefono No puede quedar vacio",false);
            return  false;
        }
        else if(txtDireccion.getText().toString().isEmpty()){
            ShowAlertDialogOK("Alerta","El Campo Direccion No puede quedar vacio",false);
            return  false;
        }
        return  true;
    }
    public void BtnBorrarCliente(View view) {
        ClsCliente cliente = new ClsCliente();
        cliente.setId(Id);
        clienteService.EliminarCliente(cliente,new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                if (databaseError != null) {
                    ShowAlertDialogOK("Error",databaseError.getMessage(),false);
                } else {
                    ShowAlertDialogOK("Eliminado Correctamente","El cliente ha sido eliminado",true);
                }

            }
        });

    }

    public void ShowAlertDialogOK(String Titulo, String Detalle, final Boolean closeActivity){
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(
                this).create();

        alertDialog.setTitle(Titulo);

        alertDialog.setMessage(Detalle);

        alertDialog.setButton(Dialog.BUTTON_NEUTRAL,"OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if(closeActivity){
                    finish();
                }

            }
        });
        alertDialog.show();
    }
    public void ShowAlertdialog(String Titulo,String detalle){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();

    }
}
