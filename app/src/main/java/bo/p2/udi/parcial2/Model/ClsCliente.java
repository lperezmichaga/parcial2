package bo.p2.udi.parcial2.Model;

public class ClsCliente {
    private String id;
    private String ci;
    private String nombre;
    private String telefono;
    private String nit;
    private String direccion;
    public  ClsCliente(){
        this.ci = "";
        this.nombre = "";
        this.telefono = "";
        this.nit = "";
        this.direccion = "";
    }

    public ClsCliente(String ci, String nombre, String telefono, String nit, String direccion) {
        this.ci = ci;
        this.nombre = nombre;
        this.telefono = telefono;
        this.nit = nit;
        this.direccion = direccion;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCi() {
        return ci;
    }

    public void setCi(String ci) {
        this.ci = ci;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
}
