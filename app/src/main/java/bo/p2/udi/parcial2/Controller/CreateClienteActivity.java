package bo.p2.udi.parcial2.Controller;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import java.util.UUID;

import bo.p2.udi.parcial2.Controller.SERVICE.ClienteService;
import bo.p2.udi.parcial2.Model.ClsCliente;
import bo.p2.udi.parcial2.R;

public class CreateClienteActivity extends AppCompatActivity {
    ClienteService clienteService;
    private EditText txtNombre,txtTelefono,txtCi,txtNit,txtDireccion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_cliente);
        initComponents();
        clienteService = new ClienteService(this);
    }
    public void initComponents(){
        txtNombre =(EditText) findViewById(R.id.txtAddNombre);
        txtCi =(EditText) findViewById(R.id.txtAddCi);
        txtTelefono =(EditText) findViewById(R.id.txtAddTelefono);
        txtNit =(EditText) findViewById(R.id.txtAddNit);
        txtDireccion =(EditText) findViewById(R.id.txtAddDireccion);
    }

    public boolean ValidarCampos(){


        if(txtNombre.getText().toString().isEmpty()){
            ShowAlertDialogOK("Alerta","El Campo Nombre No puede quedar vacio",false);
            return  false;
        }
        else if(txtCi.getText().toString().isEmpty()){
            ShowAlertDialogOK("Alerta","El Campo Ci No puede quedar vacio",false);
            return  false;
        }
        else if(txtTelefono.getText().toString().isEmpty()){
            ShowAlertDialogOK("Alerta","El Campo Telefono No puede quedar vacio",false);
            return  false;
        }
        else if(txtDireccion.getText().toString().isEmpty()){
            ShowAlertDialogOK("Alerta","El Campo Direccion No puede quedar vacio",false);
            return  false;
        }
        return  true;
    }
    public void CreateClienteBtnAgregar(View view) {
        ClsCliente objCliente = new ClsCliente();

        objCliente.setNombre(txtNombre.getText().toString());
        objCliente.setCi(txtCi.getText().toString());
        objCliente.setNit(txtNit.getText().toString());
        objCliente.setDireccion(txtDireccion.getText().toString());
        objCliente.setTelefono(txtTelefono.getText().toString());
        if(ValidarCampos()) {
            SendFireBase(objCliente);
        }
    }
    public void SendFireBase(ClsCliente objCliente){
        clienteService.AddCliente(objCliente, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                if (databaseError != null) {
                   ShowAlertDialogOK("Error", databaseError.getMessage(),false);
                } else {
                    ShowAlertDialogOK("Agregado Correctamente","Registro correcto",true);
                }
            }

        });

    }

    public void ShowAlertDialogOK(String Titulo, String Detalle, final Boolean closeActivity){
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(
                this).create();

        alertDialog.setTitle(Titulo);

        alertDialog.setMessage(Detalle);

        alertDialog.setButton(Dialog.BUTTON_NEUTRAL,"OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if(closeActivity) {
                    finish();
                }
            }
        });
        alertDialog.show();
    }

}
