package bo.p2.udi.parcial2.Model;

import com.google.android.gms.common.api.Api;

import java.util.ArrayList;
import java.util.List;

public class ListaCliente {
    List<ClsCliente> clientList;

    public ListaCliente(){
        clientList = new ArrayList<>();
    }
    public List<ClsCliente> getClientList() {
        return clientList;
    }

    public void RemoveItem(ClsCliente cliente){
        for (int i = 0; i < clientList.size(); i++) {
            ClsCliente clientl = clientList.get(i);
            if(clientl.getId().equals(cliente.getId())){
                clientList.remove(i);
            }
        }
    }
    public void ActualizarItem(ClsCliente cliente){
        for (int i = 0; i < clientList.size(); i++) {
            ClsCliente clientl = clientList.get(i);
            if(clientl.getId().equals(cliente.getId())){
                clientList.set(i,cliente);
            }
        }
    }
    public void setClientList(List<ClsCliente> clientList) {
        this.clientList = clientList;
    }
}
