package bo.p2.udi.parcial2.Controller.SERVICE;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.common.api.Api;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import java.util.UUID;

import bo.p2.udi.parcial2.Controller.FIREBASE.FirebaseController;
import bo.p2.udi.parcial2.Model.ClsCliente;

public class ClienteService {
    private Context context;
    private FirebaseController firebaseController;
    public  ClienteService(Context context){
        this.context = context;
        firebaseController = new FirebaseController(context);
    }
    public ClienteService(){
        firebaseController = new FirebaseController();
    }
    public void GetListClienteListener(ChildEventListener listener){
        firebaseController.getDatabaseReference().child("Cliente").addChildEventListener(listener);
    }
    public void AddCliente(ClsCliente objCliente,DatabaseReference.CompletionListener listener){
        objCliente.setId(UUID.randomUUID().toString());
        firebaseController.getDatabaseReference().child("Cliente").child(objCliente.getId()).setValue(objCliente,listener);
    }
    /*public void AddCliente(ClsCliente objCliente, OnSuccessListener<Void> listenersuccess, OnFailureListener failureListener){

        firebaseController.getDatabaseReference().child("Cliente").child(objCliente.getId()).setValue(objCliente).addOnSuccessListener(listenersuccess).addOnFailureListener(failureListener);
    }*/
    public void ActualizarClient(ClsCliente cliente,DatabaseReference.CompletionListener listener){
        firebaseController.getDatabaseReference().child("Cliente").child(cliente.getId()).setValue(cliente, listener);

    }

    public void EliminarCliente(ClsCliente cliente,DatabaseReference.CompletionListener listener) {
        firebaseController.getDatabaseReference().child("Cliente").child(cliente.getId()).removeValue(listener);

    }

}
