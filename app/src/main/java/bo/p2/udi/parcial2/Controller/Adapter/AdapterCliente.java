package bo.p2.udi.parcial2.Controller.Adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import bo.p2.udi.parcial2.Controller.Interface.RecyclerView_ClickListener;
import bo.p2.udi.parcial2.Model.ClsCliente;
import bo.p2.udi.parcial2.Model.ListaCliente;
import bo.p2.udi.parcial2.R;

public class AdapterCliente extends  RecyclerView.Adapter<AdapterCliente.ViewHolder>{
    public Context context;
    public ListaCliente ListaAgenda;
    private RecyclerView_ClickListener clickListener;
    public AdapterCliente(Context context, ListaCliente catList){
        this.context = context;
        this.ListaAgenda = catList;
    }

    @Override
    public AdapterCliente.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_cliente, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.Nombre.setText(ListaAgenda.getClientList().get(position).getNombre());
        holder.Ci.setText(ListaAgenda.getClientList().get(position).getCi());
        holder.Nit.setText(ListaAgenda.getClientList().get(position).getNit());
    }


    @Override
    public int getItemCount() {
        if (ListaAgenda != null)
            return ListaAgenda.getClientList().size();
        return 0;

    }

    public void setClickListener(RecyclerView_ClickListener itemClickListener) {

        this.clickListener = itemClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView Nombre,Ci;
        public TextView Nit;
        public CardView contenedor;
        public ViewHolder(View itemView) {
            super(itemView);

            Nombre = (TextView) itemView.findViewById(R.id.txtclientenombre);
            Ci = (TextView) itemView.findViewById(R.id.txtclienteci);
            Nit = (TextView) itemView.findViewById(R.id.txtclientenit);
            contenedor = (CardView) itemView.findViewById(R.id.flcontainer);
            contenedor.setTag(contenedor);
            contenedor.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }
    }
}
